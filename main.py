from thrift.transport import TTransport, TSocket
from thrift.protocol import TBinaryProtocol
import sys
sys.path.append('gen-py')
from Grafo import Handler
import random
from datetime import datetime
from termcolor import colored
import atexit

'''
11511BSI276 - Caio Eduardo Borges Assis
11421BSI264 - Victor Gomes Arantes
'''


try:
    host = sys.argv[1]
    port = sys.argv[2]
except:
    host = 'localhost'
    port = 7070

try:
    transport = TSocket.TSocket(host, port)
    transport.open()
    transport = TTransport.TBufferedTransport(transport)
    protocol = TBinaryProtocol.TBinaryProtocol(transport)
    client = Handler.Client(protocol)
except TTransport.TTransportException:
    print(colored('Não foi possível estabelecer conexão com o servidor.', 'red'))
    exit(0)
except Exception as e:
    print(e)
    exit(0)



try: # Popular com Pessoas (cor 0), Grupos (cor 1) e Recados (cor 2)
    client.createVertice(Handler.Vertice(nome=1, cor=0, desc='Joao', peso=0))
    client.createVertice(Handler.Vertice(nome=2, cor=0, desc='Maria', peso=0))
    client.createVertice(Handler.Vertice(nome=3, cor=0, desc='Pedro', peso=0))
    client.createVertice(Handler.Vertice(nome=4, cor=0, desc='Lucas', peso=0))
    client.createVertice(Handler.Vertice(nome=5, cor=0, desc='Rafael', peso=0))
    client.createVertice(Handler.Vertice(nome=6, cor=0, desc='Patricia', peso=0))
    client.createVertice(Handler.Vertice(nome=7, cor=0, desc='Marilia', peso=0))
    client.createVertice(Handler.Vertice(nome=8, cor=0, desc='Carolina', peso=0))
    client.createVertice(Handler.Vertice(nome=9, cor=0, desc='Bruna', peso=0))
    client.createVertice(Handler.Vertice(nome=10, cor=1, desc='Marvel', peso=0))
    client.createVertice(Handler.Vertice(nome=11, cor=1, desc='DC', peso=0))
    client.createVertice(Handler.Vertice(nome=12, cor=2, desc='Ola, bem vindo ao Orkut', peso=0))
    client.createVertice(Handler.Vertice(nome=13, cor=2, desc='Feliz Aniversario', peso=0))
    client.createAresta(Handler.Aresta(vertice1=1, vertice2=2, peso=0, direc=True, desc=datetime.now().strftime('%d/%m/%Y às %H:%M')))
    client.createAresta(Handler.Aresta(vertice1=1, vertice2=3, peso=0, direc=True, desc=datetime.now().strftime('%d/%m/%Y às %H:%M')))
    client.createAresta(Handler.Aresta(vertice1=1, vertice2=4, peso=0, direc=True, desc=datetime.now().strftime('%d/%m/%Y às %H:%M')))
    client.createAresta(Handler.Aresta(vertice1=1, vertice2=5, peso=0, direc=True, desc=datetime.now().strftime('%d/%m/%Y às %H:%M')))
    client.createAresta(
        Handler.Aresta(vertice1=1, vertice2=6, peso=0, direc=True, desc=datetime.now().strftime('%d/%m/%Y às %H:%M')))
    client.createAresta(
        Handler.Aresta(vertice1=2, vertice2=3, peso=0, direc=True, desc=datetime.now().strftime('%d/%m/%Y às %H:%M')))
    client.createAresta(
        Handler.Aresta(vertice1=2, vertice2=4, peso=0, direc=True, desc=datetime.now().strftime('%d/%m/%Y às %H:%M')))
    client.createAresta(
        Handler.Aresta(vertice1=2, vertice2=5, peso=0, direc=True, desc=datetime.now().strftime('%d/%m/%Y às %H:%M')))
    client.createAresta(
        Handler.Aresta(vertice1=2, vertice2=7, peso=0, direc=True, desc=datetime.now().strftime('%d/%m/%Y às %H:%M')))
    client.createAresta(
        Handler.Aresta(vertice1=2, vertice2=8, peso=0, direc=True, desc=datetime.now().strftime('%d/%m/%Y às %H:%M')))
    client.createAresta(
        Handler.Aresta(vertice1=5, vertice2=6, peso=0, direc=True, desc=datetime.now().strftime('%d/%m/%Y às %H:%M')))
    client.createAresta(
        Handler.Aresta(vertice1=8, vertice2=9, peso=0, direc=True, desc=datetime.now().strftime('%d/%m/%Y às %H:%M')))
    client.createAresta(
        Handler.Aresta(vertice1=10, vertice2=1, peso=0, direc=True, desc=datetime.now().strftime('%d/%m/%Y às %H:%M')))
    client.createAresta(
        Handler.Aresta(vertice1=10, vertice2=4, peso=0, direc=True, desc=datetime.now().strftime('%d/%m/%Y às %H:%M')))
    client.createAresta(
        Handler.Aresta(vertice1=10, vertice2=6, peso=0, direc=True, desc=datetime.now().strftime('%d/%m/%Y às %H:%M')))
    client.createAresta(
        Handler.Aresta(vertice1=11, vertice2=1, peso=0, direc=True, desc=datetime.now().strftime('%d/%m/%Y às %H:%M')))
    client.createAresta(
        Handler.Aresta(vertice1=12, vertice2=2, peso=0, direc=True, desc=datetime.now().strftime('%d/%m/%Y às %H:%M')))
    client.createAresta(
        Handler.Aresta(vertice1=13, vertice2=2, peso=0, direc=True, desc=datetime.now().strftime('%d/%m/%Y às %H:%M')))
except Exception as e:
    print(e)
    pass

option = -1
while option != 0:
    print(colored('Bem vindo ao %s' %(colored('Orkut!', 'magenta')), 'blue'))
    print(colored('\nNavegue entre as opções do menu.', 'cyan'))
    print(colored('\n1) Pessoas', 'cyan'))
    print(colored('2) Relações.', 'cyan'))
    print(colored('3) Grupos.', 'cyan'))
    print(colored('4) Recados.\nOu pressione 0 para sair.\n\n', 'cyan'))
    while option not in (0, 1, 2, 3, 4):
        try:
            option = int(input('Opção: '))
        except ValueError:
            print(colored('Por favor, selecione uma opção válida.', 'yellow'))
    if option == 0: break
    elif option == 1: # Menu Pessoas
        print(colored('Menu de Pessoas', 'blue'))
        print(colored('\nNavegue entre as opções do menu.\n', 'cyan'))
        print(colored('1) Adicionar uma Pessoa.', 'cyan'))
        print(colored('2) Remover uma Pessoa.', 'cyan'))
        print(colored('3) Atualizar o nome de uma pessoa.', 'cyan'))
        print(colored('4) Listar todas as pessoas.', 'cyan'))
        print(colored('Ou pressione 0 para voltar ao menu anterior.\n\n', 'cyan'))
        option = -1
        while option not in (0, 1, 2, 3, 4):
            try:
                option = int(input('Opção: '))
            except ValueError:
                print(colored('Por favor, selecione uma opção válida.', 'yellow'))
        if option in (1, 2, 3):
            name = input('Digite o nome: ')
        if option == 1: # Adicionar uma pessoa
            tries = 0
            op_success = False
            while not op_success:
                if tries == 5:
                    break
                try:
                    op_success = client.createVertice(Handler.Vertice(nome=random.randint(1, 401), cor=0, desc=name, peso=0))
                    if not op_success:
                        tries += 1
                except:
                    tries += 1
            if op_success:
                print(colored('%s adicionado com sucesso.' % (name), 'green'))
            else:
                print(colored('Não foi possível adicionar %s. Excedida quantidade máxima de tentativas. Tente novamente.' % (name), 'red'))
        elif option == 2: # Remover uma pessoa
            try:
                vertices = client.listVerticesDoGrafo()
            except:
                vertices = []
            matched_vertex = None
            for vertex in vertices:
                if vertex.desc == name and vertex.cor == 0:
                    matched_vertex = vertex
                    break
            if not matched_vertex:
                print(colored('Não encontrado.', 'yellow'))
            else:
                try:
                    vertex = client.readVertice(matched_vertex.nome)
                except:
                    print(colored('Pessoa não cadastrada.', 'yellow'))
                else:
                    answer = ''
                    while answer.lower() not in ('sim', 'nao'):
                        answer = input('Deseja realmente remover? (sim/nao): ')
                        if answer.lower() not in ('sim', 'nao'):
                            print(colored('Por favor, digite sim para confirmar ou nao para cancelar.', 'yellow'))
                    if answer == 'sim':
                        tries = 0
                        op_success = False
                        while not op_success:
                            if tries == 5:
                                break
                            try:
                                op_success = client.deleteVertice(matched_vertex.nome)
                                if not op_success:
                                    tries += 1
                            except:
                                tries += 1
                        if op_success:
                            print(colored('%s removido com sucesso.' % (name), 'green'))
                        else:
                            print(colored('Não foi possível remover %s. Excedida quantidade máxima de tentativas. Tente novamente.' % (name), 'red'))
                    else:
                        print(colored('Operação cancelada.', 'yellow'))
        elif option == 3: # Atualizar uma pessoa
            try:
                vertices = client.listVerticesDoGrafo()
            except:
                vertices = []
            matched_vertex = None
            for vertex in vertices:
                if vertex.desc == name and vertex.cor == 0:
                    matched_vertex = vertex
                    break
            if not matched_vertex:
                print(colored('%s não encontrado.' % (name), 'yellow'))
            else:
                try:
                    vertex = client.readVertice(matched_vertex.nome)
                except:
                    print(colored('Pessoa não cadastrada.', 'yellow'))
                else:
                    new_name = input('Digite o novo nome: ')
                    answer = ''
                    while answer.lower() not in ('sim', 'nao'):
                        answer = input('Deseja realmente alterar o nome de %s para %s? (sim/nao): ' %(name, new_name))
                        if answer.lower() not in ('sim', 'nao'):
                            print(colored('Por favor, digite sim para confirmar ou nao para cancelar.', 'yellow'))
                    if answer == 'sim':
                        tries = 0
                        op_success = False
                        while not op_success:
                            if tries == 5:
                                break
                            try:
                                matched_vertex.desc = new_name
                                op_success = client.updateVertice(matched_vertex)
                                if not op_success:
                                    tries += 1
                            except:
                                tries += 1
                        if op_success:
                            print(colored('%s (%s) alterado com sucesso.' % (new_name, name), 'green'))
                        else:
                            print(colored('Não foi possível realizar a alteração. Excedida quantidade máxima de tentativas. Tente novamente.', 'red'))
                    else:
                        print(colored('Operação cancelada.', 'yellow'))
        elif option == 4: # Listar todas as pessoas
            try:
                vertices = client.listVerticesDoGrafo()
            except:
                vertices = []
            if not vertices:
                print(colored('Não há pessoas cadastradas.', 'yellow'))
            else:
                print(colored('Lista das pessoas cadastradas:', 'blue'))
                vertices_list = []
                for vertex in vertices:
                    if vertex.cor == 0:
                        vertices_list.append(vertex.desc)
                vertices = vertices_list
                vertices.sort()
                for vertex in enumerate(vertices):
                    print(colored('%s. %s' %(int(vertex[0]) + 1, vertex[1]), 'cyan'))
                print(colored('Total: %s pessoas.' % (len(vertices)), 'yellow'))
        else: # option = 0
            pass
        print('\n\n')
    elif option == 2: # Menu Relações
        print(colored('Menu de Relações', 'blue'))
        print(colored('\nNavegue entre as opções do menu.\n', 'cyan'))
        print(colored('1) Estabelecer amizade entre 2 pessoas.', 'cyan'))
        print(colored('2) Desfazer amizade entre 2 pessoas.', 'cyan'))
        print(colored('3) Listar amizades de uma pessoa.', 'cyan'))
        print(colored('4) Listar amizades em comum entre 2 pessoas.', 'cyan'))
        print(colored('Ou pressione 0 para voltar ao menu anterior.\n\n', 'cyan'))
        option = -1
        while option not in (0, 1, 2, 3, 4):
            try:
                option = int(input('Opção: '))
            except ValueError:
                print(colored('Por favor, selecione uma opção válida.', 'yellow'))
        if option == 1: # Estabelecer amizade entre 2 pessoas
            name1 = input('Digite o nome da primeira pessoa: ')
            name2 = input('Digite o nome da segunda pessoa: ')
            try:
                vertices = client.listVerticesDoGrafo()
            except:
                vertices = []
            matched_vertex1 = matched_vertex2 = None
            for vertex in vertices:
                if vertex.desc == name1 and vertex.cor == 0:
                    matched_vertex1 = vertex
                if vertex.desc == name2 and vertex.cor == 0:
                    matched_vertex2 = vertex

            if not matched_vertex1:
                print(colored('%s não encontrado.' % (name1), 'yellow'))
            if not matched_vertex2:
                print(colored('%s não encontrado.' % (name2), 'yellow'))
            else:
                try:
                    vertex1 = client.readVertice(matched_vertex1.nome)
                    vertex2 = client.readVertice(matched_vertex2.nome)
                except:
                    print(colored('Pessoa não cadastrada.', 'yellow'))
                else:
                    tries = 0
                    op_success = False
                    while not op_success:
                        if tries == 5:
                            break
                        try:
                            op_success = client.createAresta(Handler.Aresta(vertice1=vertex1.nome, vertice2=vertex2.nome, peso=0, direc=True, desc=datetime.now().strftime('%d/%m/%Y às %H:%M')))
                            if not op_success:
                                tries += 1
                        except:
                            tries += 1
                    if op_success:
                        print(colored('Amizade entre %s e %s estabelecida com sucesso.' % (vertex1.desc, vertex2.desc), 'green'))
                    else:
                        print(colored('Não foi possível estabelecer amizade entre %s e %s. Excedida quantidade máxima de tentativas. Tente novamente.' % (vertex1.desc, vertex2.desc), 'red'))
        elif option == 2: # Desfazer amizade entre 2 pessoas
            name1 = input('Digite o nome da primeira pessoa: ')
            name2 = input('Digite o nome da segunda pessoa: ')
            try:
                vertices = client.listVerticesDoGrafo()
            except:
                vertices = []
            matched_vertex1 = matched_vertex2 = None
            for vertex in vertices:
                if vertex.desc == name1 and vertex.cor == 0:
                    matched_vertex1 = vertex
                if vertex.desc == name2 and vertex.cor == 0:
                    matched_vertex2 = vertex

            if not matched_vertex1:
                print(colored('%s não encontrado.' % (name1), 'yellow'))
            if not matched_vertex2:
                print(colored('%s não encontrado.' % (name2), 'yellow'))
            else:
                try:
                    vertex1 = client.readVertice(matched_vertex1.nome)
                    vertex2 = client.readVertice(matched_vertex2.nome)
                except:
                    print(colored('Pessoa não cadastrada.', 'yellow'))
                else:
                    tries = 0
                    op_success = False
                    while not op_success:
                        if tries == 5:
                            break
                        try:
                            op_success1 = client.deleteAresta(vertex1.nome, vertex2.nome)
                            op_success2 = client.deleteAresta(vertex2.nome, vertex1.nome)
                            op_success = op_success1 or op_success2
                            if not op_success:
                                tries += 1
                        except:
                            tries += 1
                    if op_success:
                        print(colored('Amizade entre %s e %s desfeita com sucesso.' % (vertex1.desc, vertex2.desc), 'green'))
                    else:
                        print(colored('Não foi possível desfazer amizade entre %s e %s. Excedida quantidade máxima de tentativas. Tente novamente.' % (vertex1.desc, vertex2.desc), 'red'))
        elif option == 3: # Listar amizades de uma pessoa
            name = input('Digite o nome: ')
            try:
                vertices = client.listVerticesDoGrafo()
            except:
                vertices = []
            matched_vertex = None
            for vertex in vertices:
                if vertex.desc == name and vertex.cor == 0:
                    matched_vertex = vertex
                    break
            if not matched_vertex:
                print(colored('Não encontrado.', 'yellow'))
            else:
                try:
                    vertex = client.readVertice(matched_vertex.nome)
                except:
                    print(colored('Pessoa não cadastrada.', 'yellow'))
                else:
                    try:
                        edges = client.listArestasDoGrafo()
                    except:
                        edges = []
                    friends_ids = []
                    for edge in edges:
                        if edge.vertice1 == vertex.nome and vertex.cor == 0:
                            friends_ids.append(edge.vertice2)
                        if edge.vertice2 == vertex.nome and vertex.cor == 0:
                            friends_ids.append(edge.vertice1)

                    friends_ids = set(friends_ids)
                    if not friends_ids:
                        print(colored('%s não possui amigos cadastrados.' % (name), 'yellow'))
                    else:
                        print(colored('Amigos de %s' % (name), 'blue'))
                        counter = 0
                        for friend_id in friends_ids:
                            try:
                                friend = client.readVertice(friend_id)
                                if friend.cor == 0:
                                    print(colored(friend.desc, 'cyan'))
                                    counter += 1
                            except:
                                continue
                        print(colored('Total: %s amigos.' %(counter), 'yellow'))

        elif option == 4: # Listar amizades em comum entre 2 pessoas
            name1 = input('Digite o nome da primeira pessoa: ')
            name2 = input('Digite o nome da segunda pessoa: ')
            try:
                vertices = client.listVerticesDoGrafo()
            except:
                vertices = []
            matched_vertex1 = matched_vertex2 = None
            for vertex in vertices:
                if vertex.desc == name1 and vertex.cor == 0:
                    matched_vertex1 = vertex
                if vertex.desc == name2 and vertex.cor == 0:
                    matched_vertex2 = vertex

            if not matched_vertex1:
                print(colored('%s não encontrado.' % (name1), 'yellow'))
            if not matched_vertex2:
                print(colored('%s não encontrado.' % (name2), 'yellow'))
            else:
                try:
                    vertex1 = client.readVertice(matched_vertex1.nome)
                    vertex2 = client.readVertice(matched_vertex2.nome)
                except:
                    print(colored('Pessoa não cadastrada.', 'yellow'))
                else:
                    try:
                        friends1_list = client.listArestasDoVertice(vertex1.nome)
                        friends2_list = client.listArestasDoVertice(vertex2.nome)
                        common_friends_id = []
                        for f1 in friends1_list:
                            for f2 in friends2_list:
                                if (f1.vertice1 == f2.vertice1 or f1.vertice1 == f2.vertice2):
                                    common_friends_id.append(f1.vertice1)
                                if (f1.vertice2 == f2.vertice1 or f1.vertice2 == f2.vertice2):
                                    common_friends_id.append(f1.vertice2)
                        common_friends_id = set(common_friends_id)
                        try:
                            common_friends_id.remove(vertex1.nome)
                        except:
                            pass
                        try:
                            common_friends_id.remove(vertex2.nome)
                        except:
                            pass
                        if not common_friends_id:
                            print(colored('Não possuem amigos em comum.', 'yellow'))
                        else:
                            print(colored('Amigos em comum', 'blue'))
                            counter = 0
                            for common_friend_id in common_friends_id:
                                try:
                                    friend = client.readVertice(common_friend_id)
                                    if friend.cor == 0:
                                        print(colored(friend.desc, 'cyan'))
                                        counter += 1
                                except:
                                    continue
                            print(colored('Total: %s amigos.' % (counter), 'yellow'))
                    except Exception as e:
                        print(e)
                        print(colored('Não foi possível realizar a operação', 'yellow'))
        else: # option == 0
            pass
        print('\n\n')
    elif option == 3:
        print(colored('Menu de Grupos', 'blue'))
        print(colored('\nNavegue entre as opções do menu.\n', 'cyan'))
        print(colored('1) Criar um grupo.', 'cyan'))
        print(colored('2) Excluir um grupo.', 'cyan'))
        print(colored('3) Adicionar pessoa a um grupo.', 'cyan'))
        print(colored('4) Remover uma pessoa de um grupo.', 'cyan'))
        print(colored('5) Listar todos os grupos.', 'cyan'))
        print(colored('6) Listar participantes de um grupo.', 'cyan'))
        print(colored('Ou pressione 0 para voltar ao menu anterior.\n\n', 'cyan'))
        option = -1
        while option not in (0, 1, 2, 3, 4, 5, 6):
            try:
                option = int(input('Opção: '))
            except ValueError:
                print(colored('Por favor, selecione uma opção válida.', 'yellow'))
        if option == 1: # Criar grupo
            group_name = input('Digite o nome do Grupo: ')
            tries = 0
            op_success = False
            while not op_success:
                if tries == 5:
                    break
                try:
                    op_success = client.createVertice(Handler.Vertice(nome=random.randint(1, 401), cor=1, desc=group_name, peso=0))
                    if not op_success:
                        tries += 1
                except:
                    tries += 1
            if op_success:
                print(colored('%s adicionado com sucesso.' % (group_name), 'green'))
            else:
                print(colored('Não foi possível adicionar %s. Excedida quantidade máxima de tentativas. Tente novamente.' % (group_name), 'red'))
        elif option == 2: # Remover Grupo
            group_name = input('Digite o nome do Grupo: ')
            try:
                vertices = client.listVerticesDoGrafo()
            except:
                vertices = []
            matched_vertex = None
            for vertex in vertices:
                if vertex.desc == group_name and vertex.cor == 1:
                    matched_vertex = vertex
                    break
            if not matched_vertex:
                print(colored('Não encontrado.', 'yellow'))
            else:
                try:
                    vertex = client.readVertice(matched_vertex.nome)
                except:
                    print(colored('Grupo não cadastrado.', 'yellow'))
                else:
                    answer = ''
                    while answer.lower() not in ('sim', 'nao'):
                        answer = input('Deseja realmente remover? (sim/nao): ')
                        if answer.lower() not in ('sim', 'nao'):
                            print(colored('Por favor, digite sim para confirmar ou nao para cancelar.', 'yellow'))
                    if answer == 'sim':
                        tries = 0
                        op_success = False
                        while not op_success:
                            if tries == 5:
                                break
                            try:
                                op_success = client.deleteVertice(matched_vertex.nome)
                                if not op_success:
                                    tries += 1
                            except:
                                tries += 1
                        if op_success:
                            print(colored('%s removido com sucesso.' % (group_name), 'green'))
                        else:
                            print(colored('Não foi possível remover %s. Excedida quantidade máxima de tentativas. Tente novamente.' % (group_name), 'red'))
                    else:
                        print(colored('Operação cancelada.', 'yellow'))
        elif option == 3: # Adicionar pessoa a um grupo
            name1 = input('Digite o nome da pessoa: ')
            name2 = input('Digite o nome do Grupo: ')
            try:
                vertices = client.listVerticesDoGrafo()
            except:
                vertices = []
            matched_vertex1 = matched_vertex2 = None
            for vertex in vertices:
                if vertex.desc == name1 and vertex.cor == 0:
                    matched_vertex1 = vertex
                if vertex.desc == name2 and vertex.cor == 1:
                    matched_vertex2 = vertex

            if not matched_vertex1:
                print(colored('%s não encontrado.' % (name1), 'yellow'))
            if not matched_vertex2:
                print(colored('%s não encontrado.' % (name2), 'yellow'))
            else:
                try:
                    vertex1 = client.readVertice(matched_vertex1.nome)
                    vertex2 = client.readVertice(matched_vertex2.nome)
                except:
                    print(colored('Pessoa/Grupo não cadastrado.', 'yellow'))
                else:
                    tries = 0
                    op_success = False
                    while not op_success:
                        if tries == 5:
                            break
                        try:
                            op_success = client.createAresta(Handler.Aresta(vertice1=vertex1.nome, vertice2=vertex2.nome, peso=0, direc=True, desc=datetime.now().strftime('%d/%m/%Y às %H:%M')))
                            if not op_success:
                                tries += 1
                        except:
                            tries += 1
                    if op_success:
                        print(colored('%s agora pertence ao grupo %s.' % (vertex1.desc, vertex2.desc), 'green'))
                    else:
                        print(colored('Não foi possível adicionar %s ao grupo %s. Excedida quantidade máxima de tentativas. Tente novamente.' % (vertex1.desc, vertex2.desc), 'red'))
        elif option == 4: # Remover uma pessoa de um grupo
            name1 = input('Digite o nome da pessoa: ')
            name2 = input('Digite o nome do grupo: ')
            try:
                vertices = client.listVerticesDoGrafo()
            except:
                vertices = []
            matched_vertex1 = matched_vertex2 = None
            for vertex in vertices:
                if vertex.desc == name1 and vertex.cor == 0:
                    matched_vertex1 = vertex
                if vertex.desc == name2 and vertex.cor == 1:
                    matched_vertex2 = vertex

            if not matched_vertex1:
                print(colored('%s não encontrado.' % (name1), 'yellow'))
            if not matched_vertex2:
                print(colored('%s não encontrado.' % (name2), 'yellow'))
            else:
                try:
                    vertex1 = client.readVertice(matched_vertex1.nome)
                    vertex2 = client.readVertice(matched_vertex2.nome)
                except:
                    print(colored('Pessoa/Grupo não cadastrado.', 'yellow'))
                else:
                    tries = 0
                    op_success = False
                    while not op_success:
                        if tries == 5:
                            break
                        try:
                            op_success1 = client.deleteAresta(vertex1.nome, vertex2.nome)
                            op_success2 = client.deleteAresta(vertex2.nome, vertex1.nome)
                            op_success = op_success1 or op_success2
                            if not op_success:
                                tries += 1
                        except:
                            tries += 1
                    if op_success:
                        print(colored('%s removido do grupo %s.' % (vertex1.desc, vertex2.desc), 'green'))
                    else:
                        print(colored('Não foi possível remover %s do grupo %s. Excedida quantidade máxima de tentativas. Tente novamente.' % (vertex1.desc, vertex2.desc), 'red'))
        elif option == 5: # Listar todos os grupos
            try:
                vertices = client.listVerticesDoGrafo()
            except:
                vertices = []
            if not vertices:
                print(colored('Não há grupos cadastrados.', 'yellow'))
            else:
                print(colored('Lista das grupos cadastrados:', 'blue'))
                vertices_list = []
                for vertex in vertices:
                    if vertex.cor == 1:
                        vertices_list.append(vertex.desc)
                vertices = vertices_list
                vertices.sort()
                for vertex in enumerate(vertices):
                    print(colored('%s. %s' %(int(vertex[0]) + 1, vertex[1]), 'cyan'))
                print(colored('Total: %s grupos.' % (len(vertices)), 'yellow'))
        elif option == 6: # Listar membros de um grupo
            name = input('Digite o nome do grupo: ')
            try:
                vertices = client.listVerticesDoGrafo()
            except:
                vertices = []
            matched_vertex = None
            for vertex in vertices:
                if vertex.desc == name and vertex.cor == 1:
                    matched_vertex = vertex
                    break
            if not matched_vertex:
                print(colored('%s não encontrado.' % (name), 'yellow'))
            else:
                try:
                    vertex = client.readVertice(matched_vertex.nome)
                except:
                    print(colored('Grupo não cadastrado.', 'yellow'))
                else:
                    try:
                        edges = client.listArestasDoVertice(vertex.nome)
                    except:
                        edges = []
                    if not edges:
                        print(colored('Não há membros neste grupo.'))
                    else:
                        for edge in edges:
                            try:
                                member_id = edge.vertice1 if edge.vertice1 != vertex.nome else edge.vertice2
                                member = client.readVertice(member_id)
                                print(colored(member.desc, 'cyan'))
                            except:
                                continue
                        print(colored('Total: %s membros.' % (len(edges)), 'yellow'))
        else:
            pass
        print('\n\n')
    elif option == 4:
        print(colored('Menu de Recados', 'blue'))
        print(colored('\nNavegue entre as opções do menu.\n', 'cyan'))
        print(colored('1) Deixar um recado para uma pessoa.', 'cyan'))
        print(colored('2) Listar recados de uma pessoa.', 'cyan'))
        print(colored('3) Deixar recado em um grupo.', 'cyan'))
        print(colored('4) Listar recados de um grupo.', 'cyan'))
        print(colored('Ou pressione 0 para voltar ao menu anterior.\n\n', 'cyan'))
        option = -1
        while option not in (0, 1, 2, 3, 4):
            try:
                option = int(input('Opção: '))
            except ValueError:
                print(colored('Por favor, selecione uma opção válida.', 'yellow'))
        if option == 1:  # Deixar um Recado para uma pessoa
            name = input('Digite o nome da Pessoa: ')
            message = input('Digite o recado: ')
            try:
                vertices = client.listVerticesDoGrafo()
            except:
                vertices = []
            matched_vertex = None
            for vertex in vertices:
                if vertex.desc == name and vertex.cor == 0:
                    matched_vertex = vertex
                    break
            if not matched_vertex:
                print(colored('Não encontrado.', 'yellow'))
            else:
                try:
                    vertex = client.readVertice(matched_vertex.nome)
                except:
                    print(colored('Pessoa não cadastrada.', 'yellow'))
                else:
                    tries = 0
                    op_success = False
                    while not op_success:
                        if tries == 5:
                            break
                        try:
                            message_id = random.randint(1, 401)
                            op_success = client.createVertice(
                                Handler.Vertice(nome=message_id, cor=2, desc=message, peso=0))
                            if not op_success:
                                tries += 1
                            op_success = client.createAresta(Handler.Aresta(vertice1=message_id, vertice2=vertex.nome, peso=0, direc=True, desc=datetime.now().strftime('%d/%m/%Y às %H:%M')))
                        except:
                            tries += 1
                    if op_success:
                        print(colored('Recado deixado para %s com sucesso.' % (name), 'green'))
                    else:
                        print(colored(
                            'Não foi possível deixar recado para %s. Excedida quantidade máxima de tentativas. Tente novamente.' % (
                                name), 'red'))
        elif option == 2:  # Listar todos os recados de uma pessoa
            name = input('Digite o nome da pessoa: ')
            try:
                vertices = client.listVerticesDoGrafo()
            except:
                vertices = []
            matched_vertex = None
            for vertex in vertices:
                if vertex.desc == name and vertex.cor == 0:
                    matched_vertex = vertex
                    break
            if not matched_vertex:
                print(colored('Não encontrado.', 'yellow'))
            else:
                try:
                    vertex = client.readVertice(matched_vertex.nome)
                except:
                    print(colored('Pessoa não cadastrada.', 'yellow'))
                else:
                    try:
                        edges = client.listArestasDoGrafo()
                    except:
                        edges = []
                    messages_ids = []
                    for edge in edges:
                        if edge.vertice1 == vertex.nome and vertex.cor == 0:
                            messages_ids.append(edge.vertice2)
                        if edge.vertice2 == vertex.nome and vertex.cor == 0:
                            messages_ids.append(edge.vertice1)

                    messages_ids = set(messages_ids)
                    if not messages_ids:
                        print(colored('%s não possui recados.' % (name), 'yellow'))
                    else:
                        print(colored('Recados de %s' % (name), 'blue'))
                        counter = 0
                        for message_id in messages_ids:
                            try:
                                message = client.readVertice(message_id)
                                if message.cor == 2:
                                    print(colored(message.desc, 'cyan'))
                                    counter += 1
                            except:
                                continue
                        print(colored('Total: %s recados.' %(counter), 'yellow'))
        elif option == 3:
            name1 = input('Digite o nome da pessoa: ')
            name2 = input('Digite o nome do grupo: ')
            try:
                vertices = client.listVerticesDoGrafo()
            except:
                vertices = []
            matched_vertex1 = matched_vertex2 = None
            for vertex in vertices:
                if vertex.desc == name1 and vertex.cor == 0:
                    matched_vertex1 = vertex
                if vertex.desc == name2 and vertex.cor == 1:
                    matched_vertex2 = vertex

            if not matched_vertex1:
                print(colored('%s não encontrado.' % (name1), 'yellow'))
            if not matched_vertex2:
                print(colored('%s não encontrado.' % (name2), 'yellow'))
            else:
                try:
                    vertex1 = client.readVertice(matched_vertex1.nome)
                    vertex2 = client.readVertice(matched_vertex2.nome)
                except:
                    print(colored('Pessoa/Grupo não cadastrado.', 'yellow'))
                else:
                    message = input('Digite o recado: ')
                    tries = 0
                    op_success = False
                    while not op_success:
                        if tries == 5:
                            break
                        try:
                            message_id = random.randint(1, 401)
                            op_success = client.createVertice(
                                Handler.Vertice(nome=message_id, cor=2, desc=message, peso=0))
                            if not op_success:
                                tries += 1
                            op_success = client.createAresta(
                                Handler.Aresta(vertice1=message_id, vertice2=vertex.nome, peso=0, direc=True,
                                               desc=datetime.now().strftime('%d/%m/%Y às %H:%M')))
                        except:
                            tries += 1
                    if op_success:
                        print(colored('Recado deixado para %s com sucesso.' % (name2), 'green'))
                    else:
                        print(colored(
                            'Não foi possível deixar recado para %s. Excedida quantidade máxima de tentativas. Tente novamente.' % (
                                name), 'red'))
        elif option == 4: # Listar Recados de um grupo
            group_name = input('Digite o nome do grupo: ')
            try:
                vertices = client.listVerticesDoGrafo()
            except:
                vertices = []
            matched_vertex = None
            for vertex in vertices:
                if vertex.desc == group_name and vertex.cor == 1:
                    matched_vertex = vertex
                    break
            if not matched_vertex:
                print(colored('Não encontrado.', 'yellow'))
            else:
                try:
                    vertex = client.readVertice(matched_vertex.nome)
                except:
                    print(colored('Grupo não cadastrado.', 'yellow'))
                else:
                    try:
                        edges = client.listArestasDoGrafo()
                    except:
                        edges = []
                    messages_ids = []
                    for edge in edges:
                        if edge.vertice1 == vertex.nome and vertex.cor == 2:
                            messages_ids.append(edge.vertice2)
                        if edge.vertice2 == vertex.nome and vertex.cor == 2:
                            messages_ids.append(edge.vertice1)

                    messages_ids = set(messages_ids)
                    if not messages_ids:
                        print(colored('%s não possui recados.' % (group_name), 'yellow'))
                    else:
                        print(colored('Recados de %s' % (group_name), 'blue'))
                        counter = 0
                        for message_id in messages_ids:
                            try:
                                message = client.readVertice(message_id)
                                if message.cor == 2:
                                    print(colored(message.desc, 'cyan'))
                                    counter += 1
                            except:
                                continue
                        print(colored('Total: %s recados.' % (counter), 'yellow'))
        else:
            pass
        print('\n\n')
    else: # Nao foi possivel utilizar. Pensamos em usar para saber a menor quantidade de amigos que conectam 2 pessoas
          # Porem o menor caminho de A -> B não retorna o mesmo de B -> A mesmo com arestas nao-direcionadas por causa
          # da implementacao utilizada na etapa 3 do outro grupo
        print('Menor caminho')
        name1 = input('Digite o nome da primeira pessoa: ')
        name2 = input('Digite o nome da segunda pessoa: ')
        try:
            vertices = client.listVerticesDoGrafo()
        except:
            vertices = []
        matched_vertex1 = matched_vertex2 = None
        for vertex in vertices:
            if vertex.desc == name1:
                matched_vertex1 = vertex
            if vertex.desc == name2:
                matched_vertex2 = vertex

        if not matched_vertex1:
            print(colored('%s não encontrado.' % (matched_vertex1), 'yellow'))
        if not matched_vertex2:
            print(colored('%s não encontrado.' % (matched_vertex2), 'yellow'))
        else:
            try:
                vertex1 = client.readVertice(matched_vertex1.nome)
                vertex2 = client.readVertice(matched_vertex2.nome)
            except:
                print(colored('Pessoa não cadastrada.', 'yellow'))
            else:
                try:
                    print(client.listMenorCaminho(vertex1.nome, vertex2.nome))
                except:
                    pass
        try:
            vertices = client.listVerticesDoGrafo()
        except:
            vertices = []

    option = -1

atexit.register(lambda _:transport.close(), print('Até logo!'))

