#
# Autogenerated by Thrift Compiler (0.10.0)
#
# DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
#
#  options string: py
#

from thrift.Thrift import TType, TMessageType, TFrozenDict, TException, TApplicationException
from thrift.protocol.TProtocol import TProtocolException
import sys

from thrift.transport import TTransport


class Vertice(object):
    """
    Attributes:
     - nome
     - cor
     - desc
     - peso
     - bloqueado
    """

    thrift_spec = (
        None,  # 0
        (1, TType.I32, 'nome', None, None, ),  # 1
        (2, TType.I32, 'cor', None, None, ),  # 2
        (3, TType.STRING, 'desc', 'UTF8', None, ),  # 3
        (4, TType.DOUBLE, 'peso', None, None, ),  # 4
        (5, TType.BOOL, 'bloqueado', None, False, ),  # 5
    )

    def __init__(self, nome=None, cor=None, desc=None, peso=None, bloqueado=thrift_spec[5][4],):
        self.nome = nome
        self.cor = cor
        self.desc = desc
        self.peso = peso
        self.bloqueado = bloqueado

    def read(self, iprot):
        if iprot._fast_decode is not None and isinstance(iprot.trans, TTransport.CReadableTransport) and self.thrift_spec is not None:
            iprot._fast_decode(self, iprot, (self.__class__, self.thrift_spec))
            return
        iprot.readStructBegin()
        while True:
            (fname, ftype, fid) = iprot.readFieldBegin()
            if ftype == TType.STOP:
                break
            if fid == 1:
                if ftype == TType.I32:
                    self.nome = iprot.readI32()
                else:
                    iprot.skip(ftype)
            elif fid == 2:
                if ftype == TType.I32:
                    self.cor = iprot.readI32()
                else:
                    iprot.skip(ftype)
            elif fid == 3:
                if ftype == TType.STRING:
                    self.desc = iprot.readString().decode('utf-8') if sys.version_info[0] == 2 else iprot.readString()
                else:
                    iprot.skip(ftype)
            elif fid == 4:
                if ftype == TType.DOUBLE:
                    self.peso = iprot.readDouble()
                else:
                    iprot.skip(ftype)
            elif fid == 5:
                if ftype == TType.BOOL:
                    self.bloqueado = iprot.readBool()
                else:
                    iprot.skip(ftype)
            else:
                iprot.skip(ftype)
            iprot.readFieldEnd()
        iprot.readStructEnd()

    def write(self, oprot):
        if oprot._fast_encode is not None and self.thrift_spec is not None:
            oprot.trans.write(oprot._fast_encode(self, (self.__class__, self.thrift_spec)))
            return
        oprot.writeStructBegin('Vertice')
        if self.nome is not None:
            oprot.writeFieldBegin('nome', TType.I32, 1)
            oprot.writeI32(self.nome)
            oprot.writeFieldEnd()
        if self.cor is not None:
            oprot.writeFieldBegin('cor', TType.I32, 2)
            oprot.writeI32(self.cor)
            oprot.writeFieldEnd()
        if self.desc is not None:
            oprot.writeFieldBegin('desc', TType.STRING, 3)
            oprot.writeString(self.desc.encode('utf-8') if sys.version_info[0] == 2 else self.desc)
            oprot.writeFieldEnd()
        if self.peso is not None:
            oprot.writeFieldBegin('peso', TType.DOUBLE, 4)
            oprot.writeDouble(self.peso)
            oprot.writeFieldEnd()
        if self.bloqueado is not None:
            oprot.writeFieldBegin('bloqueado', TType.BOOL, 5)
            oprot.writeBool(self.bloqueado)
            oprot.writeFieldEnd()
        oprot.writeFieldStop()
        oprot.writeStructEnd()

    def validate(self):
        return

    def __repr__(self):
        L = ['%s=%r' % (key, value)
             for key, value in self.__dict__.items()]
        return '%s(%s)' % (self.__class__.__name__, ', '.join(L))

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

    def __ne__(self, other):
        return not (self == other)


class Aresta(object):
    """
    Attributes:
     - vertice1
     - vertice2
     - peso
     - direc
     - desc
    """

    thrift_spec = (
        None,  # 0
        (1, TType.I32, 'vertice1', None, None, ),  # 1
        (2, TType.I32, 'vertice2', None, None, ),  # 2
        (3, TType.DOUBLE, 'peso', None, None, ),  # 3
        (4, TType.BOOL, 'direc', None, None, ),  # 4
        (5, TType.STRING, 'desc', 'UTF8', None, ),  # 5
    )

    def __init__(self, vertice1=None, vertice2=None, peso=None, direc=None, desc=None,):
        self.vertice1 = vertice1
        self.vertice2 = vertice2
        self.peso = peso
        self.direc = direc
        self.desc = desc

    def read(self, iprot):
        if iprot._fast_decode is not None and isinstance(iprot.trans, TTransport.CReadableTransport) and self.thrift_spec is not None:
            iprot._fast_decode(self, iprot, (self.__class__, self.thrift_spec))
            return
        iprot.readStructBegin()
        while True:
            (fname, ftype, fid) = iprot.readFieldBegin()
            if ftype == TType.STOP:
                break
            if fid == 1:
                if ftype == TType.I32:
                    self.vertice1 = iprot.readI32()
                else:
                    iprot.skip(ftype)
            elif fid == 2:
                if ftype == TType.I32:
                    self.vertice2 = iprot.readI32()
                else:
                    iprot.skip(ftype)
            elif fid == 3:
                if ftype == TType.DOUBLE:
                    self.peso = iprot.readDouble()
                else:
                    iprot.skip(ftype)
            elif fid == 4:
                if ftype == TType.BOOL:
                    self.direc = iprot.readBool()
                else:
                    iprot.skip(ftype)
            elif fid == 5:
                if ftype == TType.STRING:
                    self.desc = iprot.readString().decode('utf-8') if sys.version_info[0] == 2 else iprot.readString()
                else:
                    iprot.skip(ftype)
            else:
                iprot.skip(ftype)
            iprot.readFieldEnd()
        iprot.readStructEnd()

    def write(self, oprot):
        if oprot._fast_encode is not None and self.thrift_spec is not None:
            oprot.trans.write(oprot._fast_encode(self, (self.__class__, self.thrift_spec)))
            return
        oprot.writeStructBegin('Aresta')
        if self.vertice1 is not None:
            oprot.writeFieldBegin('vertice1', TType.I32, 1)
            oprot.writeI32(self.vertice1)
            oprot.writeFieldEnd()
        if self.vertice2 is not None:
            oprot.writeFieldBegin('vertice2', TType.I32, 2)
            oprot.writeI32(self.vertice2)
            oprot.writeFieldEnd()
        if self.peso is not None:
            oprot.writeFieldBegin('peso', TType.DOUBLE, 3)
            oprot.writeDouble(self.peso)
            oprot.writeFieldEnd()
        if self.direc is not None:
            oprot.writeFieldBegin('direc', TType.BOOL, 4)
            oprot.writeBool(self.direc)
            oprot.writeFieldEnd()
        if self.desc is not None:
            oprot.writeFieldBegin('desc', TType.STRING, 5)
            oprot.writeString(self.desc.encode('utf-8') if sys.version_info[0] == 2 else self.desc)
            oprot.writeFieldEnd()
        oprot.writeFieldStop()
        oprot.writeStructEnd()

    def validate(self):
        return

    def __repr__(self):
        L = ['%s=%r' % (key, value)
             for key, value in self.__dict__.items()]
        return '%s(%s)' % (self.__class__.__name__, ', '.join(L))

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

    def __ne__(self, other):
        return not (self == other)


class Id(object):
    """
    Attributes:
     - nome1
     - nome2
    """

    thrift_spec = (
        None,  # 0
        (1, TType.I32, 'nome1', None, None, ),  # 1
        (2, TType.I32, 'nome2', None, None, ),  # 2
    )

    def __init__(self, nome1=None, nome2=None,):
        self.nome1 = nome1
        self.nome2 = nome2

    def read(self, iprot):
        if iprot._fast_decode is not None and isinstance(iprot.trans, TTransport.CReadableTransport) and self.thrift_spec is not None:
            iprot._fast_decode(self, iprot, (self.__class__, self.thrift_spec))
            return
        iprot.readStructBegin()
        while True:
            (fname, ftype, fid) = iprot.readFieldBegin()
            if ftype == TType.STOP:
                break
            if fid == 1:
                if ftype == TType.I32:
                    self.nome1 = iprot.readI32()
                else:
                    iprot.skip(ftype)
            elif fid == 2:
                if ftype == TType.I32:
                    self.nome2 = iprot.readI32()
                else:
                    iprot.skip(ftype)
            else:
                iprot.skip(ftype)
            iprot.readFieldEnd()
        iprot.readStructEnd()

    def write(self, oprot):
        if oprot._fast_encode is not None and self.thrift_spec is not None:
            oprot.trans.write(oprot._fast_encode(self, (self.__class__, self.thrift_spec)))
            return
        oprot.writeStructBegin('Id')
        if self.nome1 is not None:
            oprot.writeFieldBegin('nome1', TType.I32, 1)
            oprot.writeI32(self.nome1)
            oprot.writeFieldEnd()
        if self.nome2 is not None:
            oprot.writeFieldBegin('nome2', TType.I32, 2)
            oprot.writeI32(self.nome2)
            oprot.writeFieldEnd()
        oprot.writeFieldStop()
        oprot.writeStructEnd()

    def validate(self):
        return

    def __repr__(self):
        L = ['%s=%r' % (key, value)
             for key, value in self.__dict__.items()]
        return '%s(%s)' % (self.__class__.__name__, ', '.join(L))

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

    def __ne__(self, other):
        return not (self == other)


class Grafo(object):
    """
    Attributes:
     - vertices
     - arestas
    """

    thrift_spec = (
        None,  # 0
        (1, TType.MAP, 'vertices', (TType.I32, None, TType.STRUCT, (Vertice, Vertice.thrift_spec), False), {
        }, ),  # 1
        (2, TType.MAP, 'arestas', (TType.STRUCT, (Id, Id.thrift_spec), TType.STRUCT, (Aresta, Aresta.thrift_spec), False), {
        }, ),  # 2
    )

    def __init__(self, vertices=thrift_spec[1][4], arestas=thrift_spec[2][4],):
        if vertices is self.thrift_spec[1][4]:
            vertices = {
            }
        self.vertices = vertices
        if arestas is self.thrift_spec[2][4]:
            arestas = {
            }
        self.arestas = arestas

    def read(self, iprot):
        if iprot._fast_decode is not None and isinstance(iprot.trans, TTransport.CReadableTransport) and self.thrift_spec is not None:
            iprot._fast_decode(self, iprot, (self.__class__, self.thrift_spec))
            return
        iprot.readStructBegin()
        while True:
            (fname, ftype, fid) = iprot.readFieldBegin()
            if ftype == TType.STOP:
                break
            if fid == 1:
                if ftype == TType.MAP:
                    self.vertices = {}
                    (_ktype1, _vtype2, _size0) = iprot.readMapBegin()
                    for _i4 in range(_size0):
                        _key5 = iprot.readI32()
                        _val6 = Vertice()
                        _val6.read(iprot)
                        self.vertices[_key5] = _val6
                    iprot.readMapEnd()
                else:
                    iprot.skip(ftype)
            elif fid == 2:
                if ftype == TType.MAP:
                    self.arestas = {}
                    (_ktype8, _vtype9, _size7) = iprot.readMapBegin()
                    for _i11 in range(_size7):
                        _key12 = Id()
                        _key12.read(iprot)
                        _val13 = Aresta()
                        _val13.read(iprot)
                        self.arestas[_key12] = _val13
                    iprot.readMapEnd()
                else:
                    iprot.skip(ftype)
            else:
                iprot.skip(ftype)
            iprot.readFieldEnd()
        iprot.readStructEnd()

    def write(self, oprot):
        if oprot._fast_encode is not None and self.thrift_spec is not None:
            oprot.trans.write(oprot._fast_encode(self, (self.__class__, self.thrift_spec)))
            return
        oprot.writeStructBegin('Grafo')
        if self.vertices is not None:
            oprot.writeFieldBegin('vertices', TType.MAP, 1)
            oprot.writeMapBegin(TType.I32, TType.STRUCT, len(self.vertices))
            for kiter14, viter15 in self.vertices.items():
                oprot.writeI32(kiter14)
                viter15.write(oprot)
            oprot.writeMapEnd()
            oprot.writeFieldEnd()
        if self.arestas is not None:
            oprot.writeFieldBegin('arestas', TType.MAP, 2)
            oprot.writeMapBegin(TType.STRUCT, TType.STRUCT, len(self.arestas))
            for kiter16, viter17 in self.arestas.items():
                kiter16.write(oprot)
                viter17.write(oprot)
            oprot.writeMapEnd()
            oprot.writeFieldEnd()
        oprot.writeFieldStop()
        oprot.writeStructEnd()

    def validate(self):
        return

    def __repr__(self):
        L = ['%s=%r' % (key, value)
             for key, value in self.__dict__.items()]
        return '%s(%s)' % (self.__class__.__name__, ', '.join(L))

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

    def __ne__(self, other):
        return not (self == other)


class NullException(TException):
    """
    Attributes:
     - mensagem
    """

    thrift_spec = (
        None,  # 0
        (1, TType.STRING, 'mensagem', 'UTF8', None, ),  # 1
    )

    def __init__(self, mensagem=None,):
        self.mensagem = mensagem

    def read(self, iprot):
        if iprot._fast_decode is not None and isinstance(iprot.trans, TTransport.CReadableTransport) and self.thrift_spec is not None:
            iprot._fast_decode(self, iprot, (self.__class__, self.thrift_spec))
            return
        iprot.readStructBegin()
        while True:
            (fname, ftype, fid) = iprot.readFieldBegin()
            if ftype == TType.STOP:
                break
            if fid == 1:
                if ftype == TType.STRING:
                    self.mensagem = iprot.readString().decode('utf-8') if sys.version_info[0] == 2 else iprot.readString()
                else:
                    iprot.skip(ftype)
            else:
                iprot.skip(ftype)
            iprot.readFieldEnd()
        iprot.readStructEnd()

    def write(self, oprot):
        if oprot._fast_encode is not None and self.thrift_spec is not None:
            oprot.trans.write(oprot._fast_encode(self, (self.__class__, self.thrift_spec)))
            return
        oprot.writeStructBegin('NullException')
        if self.mensagem is not None:
            oprot.writeFieldBegin('mensagem', TType.STRING, 1)
            oprot.writeString(self.mensagem.encode('utf-8') if sys.version_info[0] == 2 else self.mensagem)
            oprot.writeFieldEnd()
        oprot.writeFieldStop()
        oprot.writeStructEnd()

    def validate(self):
        return

    def __str__(self):
        return repr(self)

    def __repr__(self):
        L = ['%s=%r' % (key, value)
             for key, value in self.__dict__.items()]
        return '%s(%s)' % (self.__class__.__name__, ', '.join(L))

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

    def __ne__(self, other):
        return not (self == other)
